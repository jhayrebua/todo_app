import React, { createContext, useState } from 'react';


export const Todos = createContext({});
function TodosContext({ children }) {

  const defaultTodos = [
    {
      todo: 'Wash your car',
      isCompleted: false,
    },
    {
      todo: 'Do your homework',
      isCompleted: false,
    },
    {
      todo: 'Take a bath',
      isCompleted: false,
    }
  ];

  const [todoList, setTodos] = useState([...defaultTodos]);
  return (
    <Todos.Provider value={{ todoList,setTodos }} >
      {children}
    </Todos.Provider>
  )
}

export default TodosContext
