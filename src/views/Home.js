import React, { useEffect, lazy, Suspense } from 'react';
import Layout from '../layout/Layout';

const Todolist = lazy(() => import('../components/Todo/Todolist'));
const TodoForm = lazy(() => import('../components/Todo/TodoForm'));

const Home = props => {
  useEffect(() => console.log('home mounted'), []);

  return (
    <div style={style.div}>
      <TodoForm />
      <Suspense fallback={<div>Loading...</div>}>
        <Todolist />
      </Suspense>
    </div>
  )
}

const style = {
  div: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '100px',
    flexDirection: 'column',
    marginBottom : '100px'
  }
}

export default Layout(Home)
