import React from 'react';

import { redirect_path } from '../config/config';
import LoginForm from '../components/Login/LoginForm';

const Login = () => {
  return (
    <div className="logincontainer">
      <LoginForm />
    </div>
  )
}

export default Login
