import { lazy } from 'react';
const Home = lazy(() => import('../views/Home'));

export const brandname_lg = { name: 'todo', suffix: 'app' }; //BRANDNAME TO BE DISPLAY WHEN ON LG MODE,MAX OF 10 CHARACTERS FOR NAME AND 5 FOR SUFFIX 
export const brandname_sm = 'todo'  //SHORTER NAME or acronym for BRANDNAME TO BE DISPLAY WHEN ON MOBILE SIZE, MAX OF 8 CHARACTERS ONLY
export const redirect_path = '/home'; //redirect path if successfully logged in
export const API = 'http://todo_app.dev/api/'; //api link

//https://ant.design/components/icon/ for icons
export const routes = [ //array for routes
  {
    component: Home,
    title: 'home',
    icon: 'home',
    view: true, //change to false if you dont want this route to be on sidebar menu
    path: '/home1'
  },
  {
    component: Home,
    title: 'home2',
    icon: 'home',
    view: true,
    path: '/home2'
  },
  {
    component: Home,
    title: 'home3',
    icon: 'home',
    view: true,
    path: '/home3'
  },
]