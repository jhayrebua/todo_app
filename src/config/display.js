import { notification } from 'antd';
export const displayErrors = err => {

  if (err.response) {
    const { response: { status, data: { message, errors } } } = err;
    if (status === 401 || status === 404) {
      notification.error({
        message
      });

      if (status === 401 && message !== 'Invalid username or password') {
        notification.error({
          message: 'Session Expired! You will be logged out very soon!'
        })

        setTimeout(() => {
          // removeToken(); //logout
        }, 3000)
      }
    }

    if (status === 422) {
      for (var prop in errors) {
        notification.error({
          message: errors[prop][0]
        });
      }
    }
  } else {
    notification.error({
      message: 'NO RESPONSE FROM THE SERVER!'
    })
  }
}

export const displayNotification = (type, desc) => {
  notification[type]({
    message: type.toString().toUpperCase(),
    description: desc
  })
}