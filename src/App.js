import React, { Suspense } from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import TodosContext from './context/TodosContext';
import UtilsContext from './context/UtilsContext';

import Login from './views/Login';
import './assets/css/Login.css';

import { routes } from './config/config';
function App() {
  return (
    <Router>
      <Switch>
        <Suspense fallback={<div>Loading...</div>}>
          <UtilsContext >
            <Route path="/" render={prop => <Login {...prop} />} exact />
            <TodosContext>
              {routes.map((data, i) => {
                return <Route
                  key={i}
                  path={data.path}
                  render={prop => <data.component {...prop} />} />
              })}
            </TodosContext>
          </UtilsContext>
        </Suspense>
      </Switch>
    </Router>
  );
}

export default App;
