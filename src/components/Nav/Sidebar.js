import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';

import { routes } from '../../config/config';
import { brandname_lg, redirect_path } from '../../config/config';

const Sidebar = ({ location: { pathname }, isMobile, setIsMobile, width }) => {

  const getWidthAndStatus = () => {

    if (width < 999) { //mobile
      if (isMobile)
        return false;
      else
        return true;
    } else {
      if (isMobile)
        return false;
      else
        return true;
    }

  }

  const date = new Date();
  const dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  return (
    <aside className="sidebar">
      <Icon
        className="hide-sidebar"
        onClick={() => setIsMobile(getWidthAndStatus())}
        type="menu"
      />
      <Link to={redirect_path} className="brand">
        <span className="logo-lg">
          <b>{brandname_lg.name.toString().substr(0, 10)}</b>{brandname_lg.suffix.toString().substr(0, 5)}
        </span>
      </Link>
      <div className="nav-date">
        <Icon type="calendar" />
        {dayNames[date.getDay()] + ', ' + (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear()}
      </div>
      <ul>
        {routes.map((data, i) => {
          if (!data.view)
            return false

          return (
            <li key={i}>
              <Link
                to={data.path}
                className={data.path === pathname ? 'active' : ''}>
                <Icon type={data.icon} />
                <span>{data.title}</span>
              </Link>
            </li>
          )
        })}
      </ul>
    </aside>
  )
}

export default Sidebar;

