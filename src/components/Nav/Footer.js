import React from 'react';

function Footer() {
  return (
    <div style={{ textAlign : 'center', lineHeight : 0 }}>
      <p>{process.env.REACT_APP_PROJECT_NAME} {process.env.REACT_APP_VERSION}</p>
      <small>&copy; 2019</small>
    </div>
  )
}

export default Footer
