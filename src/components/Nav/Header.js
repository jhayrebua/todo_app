import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Icon } from 'antd';

import { brandname_sm, redirect_path } from '../../config/config';
import { logout } from '../../config/token';

const Header = ({ isMobile, setIsMobile, width }) => {

  const getWidthAndStatus = () => {

    if (width < 999) { //mobile
      if (isMobile)
        return false;
      else
        return true;
    } else {
      if (isMobile)
        return false;
      else
        return true;
    }

  }

  return (
    <header className="header">
      <div className="brand-sm">
        <Link to={redirect_path} className="logo-sm">
          <span>
            <b>{brandname_sm.toString().substr(0, 8)}</b>
          </span>
        </Link>
        <Icon
          style={{ marginLeft: '8px', cursor: 'pointer' }}
          onClick={() => setIsMobile(getWidthAndStatus())}
          type="menu"
        />
      </div>
      <nav className="right-nav">
        <ul>
          <li>Hi Guest!</li>
          <li>
            <Link
              onClick={() => logout()}
              style={{ color: 'white' }} to="/">Logout
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header
