import React, { Fragment, useContext, createRef } from 'react';
import { Todos } from '../../context/TodosContext';
import { Input, Button, notification } from 'antd';

const TodoForm = () => {

  const { todoList, setTodos } = useContext(Todos);
  const todoInput = createRef();

  const addTodo = e => {
    e.preventDefault();

    const todo = todoInput.current.state.value;
    todoInput.current.state.value = '';
    todoInput.current.focus();
    if (todo === undefined || todo.trim() === '') {
      notification.error({
        message : 'Please input todo task',
        duration : 1
      })
      return false;
    }

    setTodos([
      ...todoList,
      {
        todo,
        isCompleted: false,
      }
    ]);

  }

  return (
    <Fragment>
      <form style={
        {
          background: '#ffffff',
          width: '50%',
          marginBottom: 20,
          borderRadius: 2,
          border: '1px solid #d9d9d9',
          padding: 20,
          display: 'flex',
          flexDirection: 'row',
        }
      }
        onSubmit={addTodo}
      >
        <Input
          ref={todoInput}
          placeholder="Input Todo" style={{ marginRight: '5px' }}
          autoFocus />
        <Button style={{ display: 'inline' }} htmlType="submit">Add</Button>
      </form>
    </Fragment>
  )
}

export default TodoForm
