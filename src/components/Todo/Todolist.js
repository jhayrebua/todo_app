import React, { useContext } from 'react';
import { Todos } from '../../context/TodosContext';
import { List, Button } from 'antd';


const Todolist = () => {
  const { todoList, setTodos } = useContext(Todos);

  const deleteTodo = key => {
    const todo = todoList;
    todo.splice(key, 1);
    setTodos([...todo])
  }

  const completeTodo = key => {

    const todo = todoList;
    todo[key].isCompleted = !todo[key].isCompleted;

    setTodos([...todo]);

  }

  const style = {
    btnDelete: {
      color: 'indianred'
    }
  }

  return (
    <List
      style={{ background: '#ffffff', width: '50%' }}
      header={<div>TODO LIST</div>}
      bordered={true}
      size="large"
      dataSource={todoList}
      renderItem={(item, key) => (
        <List.Item
          key={key}
          actions={
            [
              item.isCompleted
                ? <Button
                  placeholder="Mark as not complete"
                  onClick={() => completeTodo(key)}
                  icon="close"></Button>
                : <Button
                  placeholder="Mark as complete"
                  onClick={() => completeTodo(key)}
                  icon="check"></Button>,
              <Button
                onClick={() => deleteTodo(key)}
                style={style.btnDelete}
                icon="delete"></Button>,
            ]
          }>
          <div style={{ marginRight: 'auto', textDecoration: item.isCompleted ? 'line-through' : 'none' }}>
            {item.todo}
          </div>
        </List.Item>
      )}
    />
  )

}

export default Todolist;